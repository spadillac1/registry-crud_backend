import { PermanentUserModel } from '../models/PermanentUser';

export const getUsers = (req, res) => {
  PermanentUserModel.find()
    .then((users) => res.json(users))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};

export const registryUsers = (req, res) => {
  const newPermanentUserModel = new PermanentUserModel(req.body);
  newPermanentUserModel
    .save()
    .then(() => res.json({ message: 'usuario agregado' }))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};

export const getUser = (req, res) => {
  const { id } = req.params;
  PermanentUserModel.findById(id)
    .then((user) => res.json(user))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};

export const deleteUser = (req, res) => {
  const { id } = req.params;
  PermanentUserModel.findByIdAndDelete(id)
    .then(() => res.json({ message: 'usuario eliminado' }))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};

export const updateUser = (req, res) => {
  const { id } = req.params;
  PermanentUserModel.findByIdAndUpdate(id, req.body)
    .then(() => res.json({ message: 'usuario eliminado' }))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};
