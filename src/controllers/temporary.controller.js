import { TemporaryUserModel } from '../models/TemporaryUser';

export const getUsers = (req, res) => {
  TemporaryUserModel.find()
    .then((users) => res.json(users))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};

export const registryUsers = (req, res) => {
  console.log(req.body);
  const newTemporaryUserModel = new TemporaryUserModel(req.body);
  newTemporaryUserModel
    .save()
    .then(() => res.json({ message: 'usuario agregado' }))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};

export const getUser = (req, res) => {
  const { id } = req.params;
  TemporaryUserModel.findById(id)
    .then((user) => res.json(user))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};

export const deleteUser = (req, res) => {
  const { id } = req.params;
  TemporaryUserModel.findByIdAndDelete(id)
    .then(() => res.json({ message: 'usuario eliminado' }))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};

export const updateUser = (req, res) => {
  const { id } = req.params;
  TemporaryUserModel.findByIdAndUpdate(id, req.body)
    .then(() => res.json({ message: 'usuario eliminado' }))
    .catch((error) => {
      console.log(error);
      res.status(401);
      res.send(error.message);
    });
};
