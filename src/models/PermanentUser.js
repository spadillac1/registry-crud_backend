import { Schema, model } from 'mongoose';

const permanentUserSchema = new Schema(
  {
    birthdate: {
      type: String,
      required: true,
    },
    homePhone: String,
    institutionalEmail: String,
    personalEmail: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    firstName: {
      type: String,
      require: true,
    },
    secondName: String,
    surname: {
      type: String,
      require: true,
    },
    secondSurname: String,
    marriedLastName: String,
    dpi: {
      type: String,
      require: true,
      unique: true,
    },
    nit: String,

    funcionalPosition: String,
    nominalPosition: {
      type: String,
      require: true,
    },
    unitWork: {
      type: String,
      require: true,
    },
  },
  { timestamps: true }
);

export const PermanentUserModel = model(
  'PermanentUserModel',
  permanentUserSchema
);
