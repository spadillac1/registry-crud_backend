import { Schema, model } from 'mongoose';

const temporaryUserSchema = new Schema(
  {
    admissionDate: { default: Date.now, type: Date, required: true },
    name: { type: String, required: true },
    phone: { type: String, required: true },
    place: { type: String, required: true },
    position: { type: String, required: true },
  },
  { timestamps: true }
);

export const TemporaryUserModel = model('TemporaryUserModel', temporaryUserSchema);
