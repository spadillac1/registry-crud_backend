import mongoose from 'mongoose';

export const startMongoose = () => {
  const properties = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  };

  mongoose.connect(
    'mongodb+srv://sam:amigo@cluster0.j8eik.mongodb.net/registry-crud?retryWrites=true&w=majority',
    properties
  );

  mongoose.connection.once('open', () => {
    console.log('Mongo DB is connected');
  });
};
