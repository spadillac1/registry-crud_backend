import dotenv from 'dotenv';
import { app } from './app';
import { startMongoose } from './database';

(() => {
  dotenv.config();
  app.listen(app.get('port'), () =>
    console.log(`Server on port:${app.get('port')}} `)
  );
  startMongoose();
})();
